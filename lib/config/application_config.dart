import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:midisynth/cubit/activity/activity_cubit.dart';

import 'package:midisynth/ui/pages/editor.dart';
import 'package:midisynth/ui/pages/home.dart';
import 'package:midisynth/ui/pages/player.dart';

class ApplicationConfig {
  // activity parameter: sequence length
  static const String parameterCodeSequenceLength = 'sequenceLength';
  static const String sequenceLengthValueShort = 'short';
  static const String sequenceLengthValueMedium = 'medium';
  static const String sequenceLengthValueLong = 'long';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexEditor = 1;
  static const int activityPageIndexPlayer = 2;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'MIDI Synth',
    activitySettings: [
      // sequence length
      ApplicationSettingsParameter(
        code: parameterCodeSequenceLength,
        values: [
          ApplicationSettingsParameterItemValue(
            value: sequenceLengthValueShort,
          ),
          ApplicationSettingsParameterItemValue(
            value: sequenceLengthValueMedium,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: sequenceLengthValueLong,
          ),
        ],
        intValueGetter: (String value) {
          const Map<String, int> intValues = {
            sequenceLengthValueShort: 8,
            sequenceLengthValueMedium: 16,
            sequenceLengthValueLong: 32,
          };
          return intValues[value] ?? 0;
        },
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexEditor);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexEditor);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      displayBottomNavBar: true,
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageHome();
              },
            );
          },
        ),
        activityPageIndexEditor: ActivityPageItem(
          code: 'page_editor',
          icon: Icon(UniconsLine.edit),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageEditor();
              },
            );
          },
        ),
        activityPageIndexPlayer: ActivityPageItem(
          code: 'page_player',
          icon: Icon(UniconsLine.play),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PagePlayer();
              },
            );
          },
        ),
      },
    ),
  );
}
