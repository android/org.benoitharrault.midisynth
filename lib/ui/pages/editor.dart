import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:midisynth/cubit/activity/activity_cubit.dart';
import 'package:midisynth/models/activity/activity.dart';

class PageEditor extends StatelessWidget {
  const PageEditor({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const SizedBox(height: 8),
            OutlinedText(
              text: '[editor]',
              fontSize: 50,
              textColor: Colors.blueAccent,
              outlineColor: Colors.blueAccent.lighten(20),
            ),
            Text(currentActivity.toString()),
          ],
        );
      },
    );
  }
}
