class Note {
  final String key;
  final String note;

  const Note({
    required this.key,
    required this.note,
  });

  @override
  String toString() {
    return '$Note(${toJson()})';
  }

  Map<String, dynamic> toJson() {
    return {
      'key': key,
      'note': note,
    };
  }
}
